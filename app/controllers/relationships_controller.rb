class RelationshipsController < ApplicationController

  before_action :logged_in_user


  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)

    # The following is for handling Ajax requests.  Either the "format.html"
    # or "format.js" line will be run -- the latter in the case of an Ajax
    # request.
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
    
  end


  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
  
  
end