Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  root             'static_pages#home'
  get 'home'    => 'static_pages#home'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  
  # In the code block below, the RESTful URLs and route names for the GET 
  # action look like this:
  
  #   URL  route name
  
  #   /users/1/following  following_user_path(1)
  #   /users/1/followers  followers_user_path(1)

  resources :users do
    member do
      get :following, :followers   # correspond to actions in users_controller.rb
    end
  end
  
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  
  # For following and unfollowing a user (create via form, destroy via form)
  resources :relationships,       only: [:create, :destroy]
  
  
  # The above "resources :users" entry adds the following actions and routes automatically:
  
  # Action Route Method
  # index users_path GET
  # show user_path(user) GET
  # new new_user_path GET
  # create users_path POST
  # edit edit_user_path(user) GET
  # update user_path(user) PATCH
  # destroy user_path(user) DELETE
  
  # The above "resources :password_resets" entry adds the following actions and RESTful routes:
  
  # METHOD - URL - Action - Named route
  
  # GET	/password_resets/new	new	new_password_reset_path
  # POST	/password_resets	create	password_resets_path
  # GET	/password_resets/<token>/edit	edit	edit_password_reset_path(token)
  # PATCH	/password_resets/<token>	update	password_reset_path(token)
  
end