# Ruby on Rails Tutorial: sample application

This is the sample application for the
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

The sample Twitter-like micropost application has been put 
together by Steve Swett by following Michael Hartl's Ruby on Rails tutorial 
-- a book over 750 pages in PDF format. The design and code are Mr. Hartl's.

The application can be seen in action [here](https://safe-peak-8398.herokuapp.com/).