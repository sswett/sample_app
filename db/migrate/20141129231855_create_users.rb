class CreateUsers < ActiveRecord::Migration
  
  # if a reversible migration can be inferred, then function "change" will work
  # otherwise a pair of functions is required: "up" and "down" -- where down
  # is a rollback.
  
  def change
    create_table :users do |t|
      t.string :name
      t.string :email

      t.timestamps null: false
    end
  end
end
